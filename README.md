# Ideas
* Tests are production code
* Cyclomatic complexity
* KISS
    * Atomicity
        * Unit vs integration
* Loader manipulation to enable mocking
* IoC